# OpenML dataset: youtube-spam-shakira

https://www.openml.org/d/42908

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Unknown 
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/YouTube+Spam+Collection) - 2017
**Please cite***: [Paper](http://dcomp.sor.ufscar.br/talmeida/youtubespamcollection/)  

**YouTube Spam Collection Shakira dataset**

It is a public set of comments collected for spam research. It has five datasets composed by 1,956 real messages extracted from five videos that were among the 10 most viewed on the collection period. This dataset only contains information about Shakira. It consists of 174 spam entries and 196 ham entries, leading to a grand total of 370samples.


### Attribute information

The collection is composed by one CSV file per dataset, where each line has the following attributes: 

COMMENT_ID,AUTHOR,DATE,CONTENT,TAG

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42908) of an [OpenML dataset](https://www.openml.org/d/42908). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42908/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42908/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42908/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

